'''
https://gitlab.com/1Chestnut/garmin-data-analysis
garmin_test.py

Description:
- Data analysis from Garmin

Requirements:
- 

Usage:
- usage: garmin_test.py

'''

import os
from datetime import date, timedelta, datetime
import configparser
from garminexporter import GarminExporter
import plotly.graph_objs as go
import plotly.io as pio
pio.renderers.default='browser'
import plotly.figure_factory as ff

def figures_to_html(figs, filename="dashboard.html"):
    dashboard = open(filename, 'w')
    dashboard.write("<html><head></head><body>" + "\n" + datetime.now().isoformat() + "\n")
    for fig in figs:
        inner_html = fig.to_html().split('<body>')[1].split('</body>')[0]
        dashboard.write(inner_html)
    dashboard.write("</body></html>" + "\n")

# Reading config file
config = configparser.ConfigParser()
config.read('config.ini')
email = config['GarminLogin']['email']
password = config['GarminLogin']['password']
date_from =  config['GarminDownload']['date_from']
daily_step_goal = float(config['GarminDownload']['daily_step_goal'])

# Download data and get DataFrame
ge = GarminExporter(email, password)
# ge.db_reset("2018-01-01")
# ge.connect()
# ge.update_db()
df = ge.export_dataframe(date_from)

# Code cleaning
# df = df.fillna(0)
df['dailyStepGoal']=daily_step_goal
df['dailyStepGoalAchieved']=df['totalSteps']>df['dailyStepGoal']

# Compute data
steps_daily = df[['totalSteps','dailyStepGoal']].resample('D').sum()
steps_weekly = df[['totalSteps','dailyStepGoal']].resample('W').sum()
# steps_monthly = df['totalSteps'].resample('M').sum()

l_figs = []
fig = go.Figure()
date_iso = (date.today()-timedelta(days=1*30)).isoformat()
df_steps = steps_daily[steps_daily.index>date_iso]
fig.add_trace(go.Bar(y=df_steps['totalSteps'], x=df_steps.index, name='Daily Steps - 1 month', marker_color='lightgreen', text=df_steps['totalSteps'], textposition='auto', orientation='v'))
fig.update_layout(title='Daily Steps - 1 months', xaxis_tickangle=-45, xaxis=dict(tickvals=df_steps.index), height=500, width=1200, legend=dict(yanchor="top", xanchor="left", y=0.99, x=0.01))
l_figs.append(fig)
fig = go.Figure()
date_iso = (date.today()-timedelta(days=3*30)).isoformat()
df_steps = steps_daily[steps_daily.index>date_iso]
fig.add_trace(go.Bar(y=df_steps['totalSteps'], x=df_steps.index, name='Daily Steps - 3 months', marker_color='lightgreen', text=df_steps['totalSteps'], textposition='auto', orientation='v'))
fig.update_layout(title='Daily Steps - 3 months', xaxis_tickangle=-45, xaxis=dict(tickvals=df_steps.index), height=500, width=1200, legend=dict(yanchor="top", xanchor="left", y=0.99, x=0.01))
l_figs.append(fig)
fig = go.Figure()
date_iso = (date.today()-timedelta(days=6*30)).isoformat()
df_steps = steps_daily[steps_daily.index>date_iso]
fig.add_trace(go.Bar(y=df_steps['totalSteps'], x=df_steps.index, name='Daily Steps - 6 months', marker_color='lightgreen', text=df_steps['totalSteps'], textposition='auto', orientation='v'))
fig.update_layout(title='Daily Steps - 6 months', xaxis_tickangle=-45, xaxis=dict(tickvals=df_steps.index), height=500, width=1200, legend=dict(yanchor="top", xanchor="left", y=0.99, x=0.01))
l_figs.append(fig)
fig = go.Figure()
date_iso = (date.today()-timedelta(days=12*30)).isoformat()
df_steps = steps_daily[steps_daily.index>date_iso]
fig.add_trace(go.Bar(y=df_steps['totalSteps'], x=df_steps.index, name='Daily Steps - 12 months', marker_color='lightgreen', text=df_steps['totalSteps'], textposition='auto', orientation='v'))
fig.update_layout(title='Daily Steps - 6 months', xaxis_tickangle=-45, xaxis=dict(tickvals=df_steps.index), height=500, width=1200, legend=dict(yanchor="top", xanchor="left", y=0.99, x=0.01))
l_figs.append(fig)

l_figs.append(ff.create_table(steps_daily.sort_values(by='totalSteps',ascending=False).head(5)))
l_figs.append(ff.create_table(steps_weekly.sort_values(by='totalSteps',ascending=False).head(5)))

figures_to_html(l_figs,'dashboard_steps.html')
