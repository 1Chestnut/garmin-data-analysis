'''
https://gitlab.com/1Chestnut/garmin-data-analysis
garminexporter.py

Description:
- Connects to Garmin and downloads wellness data. Exports data as DataFrame

Requirements:
- garminconnect library: pip3 install garminconnect

Usage:
- 

'''

# import logging
# logging.basicConfig(level=logging.DEBUG)
import sys, os, shutil
import configparser
import json
import pandas as pd
from garminconnect import (Garmin, GarminConnectConnectionError, GarminConnectTooManyRequestsError, GarminConnectAuthenticationError)
from datetime import date,timedelta


class GarminExporter:
    
    DB_PATH = "./db/"
    
    _email = ""
    _password = ""
    _client = ""
    
    '''
    Initialise Garmin object with credentials. Only needed once.
    '''
    def __init__ (self, email, password):
        self._email = email
        self._password = password
        
        try:
            self._client = Garmin(self._email, self._password)
        except (GarminConnectConnectionError, GarminConnectAuthenticationError, GarminConnectTooManyRequestsError) as err:
            print("Error occurred during Garmin Connect Client init: %s" % err)
            sys.exit(1)
        except Exception:  # pylint: disable=broad-except
            print("Unknown error occurred during Garmin Connect Client init")
            sys.exit(1)
        
    '''
    Login to Garmin Connect portal. Only needed at start of your program. The library will try to relogin when session expires
    '''
    def connect (self):
        try:
            self._client.login()
        except (GarminConnectConnectionError, GarminConnectAuthenticationError, GarminConnectTooManyRequestsError) as err:
            print("Error occurred during Garmin Connect Client login: %s" % err)
            sys.exit(1)
        except Exception:  # pylint: disable=broad-except
            print("Unknown error occurred during Garmin Connect Client login")
            sys.exit(1)
        print("Connected to Garmin Connect.")

    '''
    Download data from Garmin from the specified date.
    '''
    def download_from_date (self, date_from_isoformat):
        date_from = date.fromisoformat(date_from_isoformat)
        while date_from < date.today():
            print("Downloading: " + date_from.isoformat())
            try:
                data = self._client.get_stats(date_from.isoformat())
                with open(self.DB_PATH + date_from.isoformat() + ".txt", 'w') as outfile:
                    json.dump(data, outfile)
                date_from += timedelta(days=1)
            except (GarminConnectConnectionError, GarminConnectAuthenticationError, GarminConnectTooManyRequestsError) as err:
                print("Error occurred during Garmin Connect Client get stats: %s" % err)
                sys.exit(1)
            except Exception:  # pylint: disable=broad-except
                print("Unknown error occurred during Garmin Connect Client get stats")
                sys.exit(1)
    
    '''
    Update DB by downloading from last known updated date
    '''
    def update_db (self):
        self.download_from_date(self._db_get_last_sync())
    
    '''
    Export dataframe from specified date.
    '''
    def export_dataframe (self, date_from_isoformat):
        # Process downloaded data
        date_from = date.fromisoformat(date_from_isoformat)
        last_updated_date = self._db_get_last_sync()
        list_dict = []
        while date_from < date.today():
            file_path = self.DB_PATH + date_from.isoformat() + ".txt"
            if (os.path.exists(file_path)):
                with open(file_path) as json_file:
                    json_data = json.load(json_file)
                    total_steps = json_data['totalSteps']
                    dict_data = {'date':pd.to_datetime(date_from),
                                 'totalSteps':json_data['totalSteps'],
                                 'dailyStepGoal':json_data['dailyStepGoal'],
                                 'highlyActiveSeconds':json_data['highlyActiveSeconds'],
                                 'activeSeconds':json_data['activeSeconds'],
                                 'sedentarySeconds':json_data['sedentarySeconds'],
                                 'moderateIntensityMinutes':json_data['moderateIntensityMinutes'],
                                 'vigorousIntensityMinutes':json_data['vigorousIntensityMinutes'],
                                 'intensityMinutesGoal':json_data['intensityMinutesGoal'],
                                 'totalKilocalories':json_data['totalKilocalories'],
                                 'activeKilocalories':json_data['activeKilocalories'],
                                 'bmrKilocalories':json_data['bmrKilocalories'],
                                 'minHeartRate':json_data['minHeartRate'],
                                 'maxHeartRate':json_data['maxHeartRate'],
                                 'restingHeartRate':json_data['restingHeartRate'],
                                 }
                    list_dict.append(dict_data)

                    if (str(total_steps) != "None"):
                        last_updated_date = date_from - timedelta(days=1)
        
            date_from += timedelta(days=1)
        
        self._db_set_last_sync(last_updated_date.isoformat())
        # Create DataFrame
        df = pd.DataFrame(list_dict)
        df = df.set_index('date', drop=True)
        return df
    
    '''
    Get last date where DB was synced
    '''
    def _db_get_last_sync (self):
        db_info = configparser.ConfigParser()
        db_info.read(self.DB_PATH+'db_info.ini')
        last_sync = db_info['SyncStatus']['last_sync']
        return last_sync
    
    '''
    Set last date when DB was synced
    '''
    def _db_set_last_sync (self, last_sync):
        db_info = configparser.ConfigParser()
        db_info['SyncStatus'] = {'last_sync' : last_sync}
        with open(self.DB_PATH+'db_info.ini', 'w+') as configfile:
            db_info.write(configfile)
    
    '''
    Reset DB to start a new one. All data is deleted.
    '''
    def db_reset (self, db_date):
        if (os.path.exists(self.DB_PATH)):
            shutil.rmtree(self.DB_PATH)
        os.mkdir(self.DB_PATH)
        
        db_info = configparser.ConfigParser()
        db_info['SyncStatus'] = {'last_sync' : db_date}
        with open(self.DB_PATH+'db_info.ini', 'w+') as configfile:
            db_info.write(configfile)
        
        
        
        
        
        
        
        
        