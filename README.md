# garmin-data-analysis

## Description
Retrieve wellness data from Garmin and plot information

## Requirements
- garminconnect library ([repo](https://github.com/cyberjunky/python-garminconnect)): pip3 install garminconnect